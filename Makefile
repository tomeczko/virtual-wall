avrType=atmega8
SRCDIR = virtual-wall
OBJDIR = obj
APP = $(SRCDIR)
F_CPU = 8000000
OPTS = -D F_CPU=$(F_CPU) -O2 -Wall -std=c99

SRCS    = $(shell find $(SRCDIR) -name '*.c')
SRCDIRS = $(shell find . -name '*.c' | dirname {} | sort | uniq | sed 's/\/$(SRCDIR)//g' )
OBJS    = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCS))

$(APP): dirs $(OBJS) elf hex

dirs:
	mkdir -p $(OBJDIR)

all: $(APP)

clean:
	rm -rf $(OBJDIR)


$(OBJDIR)/%.o: $(SRCDIR)/%.c
	avr-gcc $(OPTS) -mmcu=$(avrType) -c $< -o $@


elf: $(OBJS)
	avr-gcc $(cflags) -mmcu=$(avrType) -o $(OBJDIR)/$(APP).elf $^ -Wl,-Map,$(OBJDIR)/$(APP).map
	chmod a-x $(OBJDIR)/$(APP).elf # 2>&1


hex:    elf
	avr-objcopy -j .text -j .data -O ihex $(OBJDIR)/$(APP).elf $(OBJDIR)/$(APP).flash.hex
	avr-objcopy -j .eeprom --set-section-flags=.eeprom="alloc,load" --change-section-lma .eeprom=0 -O ihex $(OBJDIR)/$(APP).elf $(OBJDIR)/$(APP).eeprom.hex
	# avr-objcopy -j .fuse -O ihex $(src).elf $(src).fuses.hex --change-section-lma .fuse=0
	# srec_cat $(src).fuses.hex -Intel -crop 0x00 0x01 -offset  0x00 -O $(src).lfuse.hex -Intel
	# srec_cat $(src).fuses.hex -Intel -crop 0x01 0x02 -offset -0x01 -O $(src).hfuse.hex -Intel
	# srec_cat $(src).fuses.hex -Intel -crop 0x02 0x03 -offset -0x02 -O $(src).efuse.hex -Intel

flash: hex
	avrdude -p m8 -c usbasp -U flash:w:$(OBJDIR)/$(APP).flash.hex

#elf: object
#	avr-gcc $(cflags) -mmcu=$(avrType) -o $(src).elf $(src).o
#	chmod a-x $(src).elf 2>&1

# all:
#% 	avr-gcc -Os -mmcu=atmega8 -c %f
#%	avr-gcc -g -mmcu=atmega8 -o %e.elf %e.o
#%	avr-objcopy -j .text -j .data -O ihex %e.elf %e.hex

