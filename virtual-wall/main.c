#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <stdbool.h>

// All following four values are in seconds
static const int SECONDS_PER_BLINK = 15*60;
static const int BLINKS_INTERVAL = 10;
static const int CLICK_INCREMENT = 15*60;
static const int INITIAL_TIMEOUT = 15*60;

static const int BUTTON_TIMEOUT = 120; // Do not modify (button sensitivity)

volatile unsigned int timer2Counter = 0;
volatile bool tenSecsInt;
volatile unsigned int buttonTimeoutCounter;
volatile unsigned int keyCounter;
volatile bool keyEvent;
volatile bool incrTimeoutByButton;

volatile unsigned int codeCounter;
volatile unsigned char code[] = {0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0};

volatile char transmitterState;

volatile long remainingSeconds;


inline void controlLed(bool state)
{
    if (state)
        PORTC &= ~(1 << 5);
    else
        PORTC |= (1 << 5);
}


inline void key(char state)
{
    if (state)
        TCCR1A |= (1 << COM1B0);
    else
        TCCR1A &= ~(1 << COM1B0);
    transmitterState = state;
}

ISR(INT0_vect)
{
	if (incrTimeoutByButton)
		return;
	cli();
	buttonTimeoutCounter = 0;
	incrTimeoutByButton = true;
	remainingSeconds += CLICK_INCREMENT;
	sei();
}

ISR( TIMER2_COMP_vect ) // Let's have a report every given time
{
    timer2Counter++;
    if (timer2Counter > (100*BLINKS_INTERVAL)/2) {
        tenSecsInt = true;
        timer2Counter = 0;
    }
    keyCounter++;
    if (keyCounter > 7) {
        keyEvent = true;
        keyCounter = 0;
    }
}

ISR(TIMER0_OVF_vect)
{
    if (!keyEvent)
        return;

    if (code[codeCounter])
        TCNT0 = 0x100 - 0x5e;
    else
        TCNT0 = 0x100 - 0x20;
    key(~transmitterState);

    codeCounter++;
    if (codeCounter > 15) {
        keyEvent = false;
        codeCounter = 0;
        key(0);
    }
}



int main()
{
    // Activate pull-up on INT0 (Port D.2)
    DDRD |= (1 << 2);
    PORTD |= (1 << 2);
    DDRD &= ~(1 << 2);

    // Configure timer1 (38kHz PWM)
    TCCR1A |= (1 << COM1B0);

    TCCR1B |= (1 << WGM12); // Turn on CTC mode
    TCCR1B |= (1 << CS10); // prescaler /1

    OCR1A = 0x006a; // 16-bit register (for 38kHz @8MHz)

    // Configure timer2
    TCCR2 |= (1 << WGM21);
    TCCR2 |= (1 << CS22) | (1 << CS21) | (1 << CS20); // prescaler /1024
    OCR2 = 0x9c; // (interrupt every 20ms @8MHz)

    TCCR0 |= (1 << CS02); // prescaler /256

    DDRC |= (1 << 5);
    DDRB |= (1 << 2);
    PORTB &= ~(1 << 2); // Set output to zero when PWM is off

    TIMSK |= (1 << OCIE2);
    TIMSK |= (1 << TOIE0);

    MCUCR |= (1 << ISC01); // Falling edge
	GICR |= (1 << INT0); // Enable INT0

	remainingSeconds = INITIAL_TIMEOUT;

	key(0);
    sei();

    controlLed(false);

    while (1) {
        if (tenSecsInt || incrTimeoutByButton) {
            if (incrTimeoutByButton) {
                timer2Counter = 0;
            }
            if (!incrTimeoutByButton) {
                remainingSeconds -= BLINKS_INTERVAL;
            }

            for (int i = 0; i <= remainingSeconds / SECONDS_PER_BLINK; i++) {
                controlLed(true);
                _delay_ms(100);
                controlLed(false);
                _delay_ms(300);
            }

            tenSecsInt = false;
            if (incrTimeoutByButton) {
                if (!(PIND &= (1 << 2))) { // Button is still pressed
                    controlLed(true);
                    _delay_ms(2000);
                    controlLed(false);
                    remainingSeconds = 0;
                }
            }
            if (incrTimeoutByButton) {
                incrTimeoutByButton = false;
            }

            if (remainingSeconds <= 0) {
                remainingSeconds = 0;
                // Go sleep
                TIMSK &= ~( 1 << TOIE0); // Turn off keying
                key(0);
                set_sleep_mode(SLEEP_MODE_PWR_DOWN);
                MCUCR &= ~(1 << ISC01 | 1 << ISC00); // Low-level
                GICR |= (1 << INT0); // Enable INT0
                sleep_mode();

                // And this happens after wake-up
                MCUCR |= (1 << ISC01); // Falling edge back
                remainingSeconds = INITIAL_TIMEOUT;
                tenSecsInt = true;
                TIMSK |= (1 << TOIE0); // Turn on keying
            }
        }
    }
}

